// Here is where the various combinators are imported. You can find all the combinators here:
// https://docs.rs/nom/5.0.1/nom/
// If you want to use it in your parser, you need to import it here. I've already imported a couple.

use nom::bytes::complete::is_not;
use nom::{
  IResult,
  branch::alt,
  combinator::opt,
  multi::{many1, many0},
  bytes::complete::{tag},
  character::complete::{alphanumeric1, digit1, newline},
  sequence::delimited,
  // dbg_dmp,
};

// Here are the different node types. You will use these to make your parser and your grammar.
// You may add other nodes as you see fit, but these are expected by the runtime.

#[derive(Debug, Clone)]
pub enum Node {
  Program { children: Vec<Node> },
  Statement { children: Vec<Node> },
  FunctionReturn { children: Vec<Node> },
  FunctionDefine { children: Vec<Node> },
  FunctionArguments { children: Vec<Node> },
  FunctionStatements { children: Vec<Node> },
  Expression { children: Vec<Node> },
  MathExpression {name: String, children: Vec<Node> },
  FunctionCall { name: String, children: Vec<Node> },
  VariableDefine { children: Vec<Node> },
  Number { value: i32 },
  Bool { value: bool },
  Identifier { value: String },
  String { value: String },
  // I can add nodes that I want
}

// Define production rules for an identifier
pub fn identifier(input: &str) -> IResult<&str, Node> {
  let (input, result) = alphanumeric1(input)?;              // Consume at least 1 alphanumeric character. The ? automatically unwraps the result if it's okay and bails if it is an error.
  Ok((input, Node::Identifier{ value: result.to_string()})) // Return the now partially consumed input, as well as a node with the string on it.
}

// Define an integer number
pub fn number(input: &str) -> IResult<&str, Node> {
  let (input, result) = digit1(input)?;                     // Consume at least 1 digit 0-9
  let number = result.parse::<i32>().unwrap();              // Parse the string result into a usize
  Ok((input, Node::Number{ value: number}))                 // Return the now partially consumed input with a number as well
}

pub fn boolean(input: &str) -> IResult<&str, Node> {
  let (input, result) = alt((tag("true"), tag("false")))(input)?;
  Ok((input, Node::Bool{ value: result.parse().unwrap()}))
}

pub fn string(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("\"")(input)?;
  let (input, result) = many0(alt((alphanumeric1, tag(" "))))(input)?;
  let (input, _) = tag("\"")(input)?;
  Ok((input, Node::String{ value: result.into_iter().collect()}))
}

pub fn function_call(input: &str) -> IResult<&str, Node> {
  let (input, funcname) = many1(alphanumeric1)(input)?;
  // let (input, result) = delimited(tag("("), is_not(")"), tag(")"))(input)?;
  // let (input, funcarguments) = alt((arguments3, arguments1 ))(result)?;
  // Ok((input, Node::FunctionCall{name: funcname.into_iter().collect(), children: vec![funcarguments]}))

  
  let (input, left_paren) = tag("(")(input)?;
  // println!("{:?}", left_paren);
  let (input, funcarguments) = alt((arguments3, arguments1, arguments))(input)?;
  // println!("{:?}", funcarguments);
  let (input, right_paren) = tag(")")(input)?;
  // println!("{:?}", right_paren);
  Ok((input, Node::FunctionCall{name: funcname.into_iter().collect(), children: vec![funcarguments]}))
}

pub fn arguments(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("")(input)?;
  Ok((input, Node::FunctionArguments{children: vec![]}))
}

pub fn arguments1(input: &str) -> IResult<&str, Node> {
  let (input, result) = expression(input)?;
  // println!("{:?}", result);
  Ok((input, Node::FunctionArguments{children: vec![result]}))
}

pub fn arguments3(input: &str) -> IResult<&str, Node> {
  let (input, result1) = expression(input)?;
  let (input, _) = tag(",")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result2) = expression(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = tag(",")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result3) = expression(input)?;
  // println!("{:?}", result3);
  Ok((input, Node::FunctionArguments{children: vec![result1, result2, result3]}))
}

// Math expressions with parens (1 * (2 + 3))
pub fn parenthetical_expression(input: &str) -> IResult<&str, Node> {
  // let (input, op) = tag("(")(input)?;
  // let (input, args) = l1(input)?;
  // let (input, _) = tag(")")(input)?;
  //Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))

  delimited(tag("("), l1, tag(")"))(input)
}

pub fn l4(input: &str) -> IResult<&str, Node> {
  alt((function_call, number, identifier, parenthetical_expression))(input)
}

pub fn l3_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = tag("^")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l4(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

pub fn l3(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l4(input)?;
  let (input, tail) = many0(l3_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn l2_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("*"),tag("/")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l3(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

pub fn l2(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l3(input)?;
  let (input, tail) = many0(l2_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

// L1 - L4 handle order of operations for math expressions 
pub fn l1_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("+"),tag("-")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l2(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))

}

pub fn l1(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l2(input)?;
  let (input, tail) = many0(l1_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn math_expression(input: &str) -> IResult<&str, Node> {
  l1(input)
}

pub fn expression(input: &str) -> IResult<&str, Node> {
  let (input, result) = alt((boolean, math_expression, function_call, number, string,  identifier))(input)?;  
  Ok((input, Node::Expression{ children: vec![result]}))
}

pub fn statement(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result) = alt((variable_define, function_return))(input)?;
  // println!("{:?}", result);
  let (input, final_colon) = tag(";")(input)?;
  // println!("{:?}", final_colon);
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(newline)(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  Ok((input, Node::Statement{ children: vec![result]}))
}

pub fn function_return(input: &str) -> IResult<&str, Node> {
  let (input, return_string) = tag("return ")(input)?;
  // println!("{:?}", return_string);
  let (input, result) = expression(input)?;
  // println!("{:?}", result);
  // let (input, _) = tag(";")(input)?;
  Ok((input, Node::FunctionReturn{ children: vec![result]}))
}

// Define a statement of the form
// let x = expression
pub fn variable_define(input: &str) -> IResult<&str, Node> {
  let (input, first_identifier) = tag("let ")(input)?;
  // println!("{:?}", first_identifier);
  let (input, variable) = identifier(input)?;
  // println!("{:?}", variable);
  let (input, white_space_1) = many0(tag(" "))(input)?;
  // println!("{:?}", white_space_1);
  let (input, equal_sign) = tag("=")(input)?;
  // println!("{:?}", equal_sign);
  let (input, white_space_2) = many0(tag(" "))(input)?;
  // println!("{:?}", white_space_2);
  let (input, expression) = expression(input)?;
  // println!("{:?}", expression);
  Ok((input, Node::VariableDefine{ children: vec![variable, expression]}))   
}

// pub fn space_newline(input: &str) -> IResult<&str, Node> {
//   many0(tag(" "))(input)
// }


// Like the first argument but with a comma in front
pub fn other_arg(input: &str) -> IResult<&str, Node> {
  unimplemented!();
}

pub fn function_definition(input: &str) -> IResult<&str, Node> {
  let (input, func_pre) = tag("fn ")(input)?;
  // println!("{:?}", func_pre);
  let (input, func_identifier) = identifier(input)?;
  // println!("{:?}", func_identifier);
  
  let (input, left_paren) = tag("(")(input)?;
  // println!("{:?}", left_paren);
  let (input, funcarguments) = alt((arguments3, arguments1, arguments))(input)?;
  // println!("{:?}", funcarguments);
  let (input, _) = tag(")")(input)?;

  // dealing with white_space and new_line
  let (input, space_after_ident) = many0(tag(" "))(input)?;
  // println!("{:?}", space_after_ident);

  let (input, left_curve_paren) = tag("{")(input)?;
  // println!("{:?}", left_curve_paren);
  let (input, space_after_ident) = many0(tag(" "))(input)?;
  let (input, new_line) = many0(newline)(input)?;
  // println!("{:?}", new_line);
  let (input, _) = many0(tag(" "))(input)?;
  let (input, funcstatements1) = statement(input)?;
  // println!("{:?}", funcstatements1);

  let mut part1 = vec![func_identifier, funcarguments, funcstatements1];

  let (input, result1) = opt(statement)(input)?;
  match result1 {
    Some(x) => {
      part1.push(x);
    },
    _ => (),
  }
  let (input, result1) = opt(statement)(input)?;
  match result1 {
    Some(x) => {
      part1.push(x);
    },
    _ => (),
  } 

  let (input, right_curve_paren) = tag("}")(input)?;
  // println!("{:?}", right_curve_paren);
  let (input, optional_ws) = many0(tag(" "))(input)?;
  // println!("{:?}", optional_ws);
  let (input, new_line) = many0(newline)(input)?;
  // println!("{:?}", new_line);
  let (input, _) = many0(tag(" "))(input)?;
  let (input, new_line) = many0(newline)(input)?;
  // println!("{:?}", new_line);
  let (input, _) = many0(tag(" "))(input)?;

  Ok((input, Node::FunctionDefine{ children: part1}))

  // Ok((input, Node::FunctionDefine{ children: vec![func_identifier, funcarguments, funcstatements1]}))
}

pub fn comment(input: &str) -> IResult<&str, Node> {
  unimplemented!();
}

// Define a program. You will change this, this is just here for example.
// You'll probably want to modify this by changing it to be that a program
// is defined as at least one function definition, but maybe more. Start
// by looking up the many1() combinator and that should get you started.
pub fn program(input: &str) -> IResult<&str, Node> {
  let (input, result) = alt((function_definition, statement, expression))(input)?;
  let mut part1 = vec![result];
  let (input, result1) = opt(function_definition)(input)?;
  match result1 {
    Some(x) => {
      part1.push(x);
    },
    _ => (),
  }
  let (input, result1) = opt(function_definition)(input)?;
  match result1 {
    Some(x) => {
      part1.push(x);
    },
    _ => (),
  } 
  Ok((input, Node::Program{ children: part1}))
  
  // cargo test successful 24
  // let (input, result) = alt((statement, expression))(input)?;  // Now that we've defined a number and an identifier, we can compose them using more combinators. Here we use the "alt" combinator to propose a choice.
  // Ok((input, Node::Program{ children: vec![result]}))       // Whether the result is an identifier or a number, we attach that to the program
}

// program is functiondefine, expression, and statement
