extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program(r#"fn foo(a,b,c) {
    let x = a + 1;
    let y = bar(c - b);
    return x * y;
  }
  
  fn bar(a) {
    return a * 3;
  }
  
  fn main() {
    return foo(1,2,3);  
  }"#);
  println!("{:?}", result);
}


// try to use nom_trace
/*
#[macro_use] extern crate nom;
#[macro_use] extern crate nom_trace;

//adds a thread local storage object to store the trace
//declare_trace!();

pub fn main() {
  named!(parser<&str, Vec<&str>>,
    //wrap a parser with tr!() to add a trace point
    tr!(preceded!(
      tr!(tag!("data: ")),
      tr!(delimited!(
        tag!("("),
        separated_list!(
          tr!(tag!(",")),
          tr!(nom::character::complete::digit1)
        ),
        tr!(tag!(")"))
      ))
    ))
  );

  println!("parsed: {:?}", parser("data: (1,2,3)"));

  // prints the last parser trace
  print_trace!();

  // the list of trace events can be cleared
  reset_trace!();
}
*/
